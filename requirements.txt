paramiko
tornado==5.1.1; python_version < '3.5'
tornado==6.1; python_version >= '3.5'
wheel
setuptools
PyQt5==5.15.2
PyQt5-sip==12.8.1
PyQt5-stubs==5.14.2.2
PyQtWebEngine==5.15.2